
/**
 * @file audir.c
 *
 * @brief AudIR library implementation
 *
 * @author Hajime Suzuki
 * @date 2015/6/27
 * @license Apache v2.
 *
 * @detail
 * The format specs are retrieved from
 * http://elm-chan.org/docs/ir_format.html on 2015/6/27.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <portaudio.h>
#include "audir.h"
#include "kvec.h"

static int const out_id_base = 1;
static int const in_id_base = 101;
static int const sps = 44100;

#define NEC_T 					( 562 )
#define NEC_INTV 				( 108000 )
#define NEC_HEADER_ONE_LEN 		( 16 )
#define NEC_HEADER_ZERO_LEN		( 8 )
#define NEC_HEADER_LEN 			( NEC_HEADER_ONE_LEN + NEC_HEADER_ZERO_LEN )
#define NEC_DEVICE_LEN 			( 16 )
#define NEC_DATA_LEN 			( 8 )
#define NEC_TRAILER_LEN		 	( 1 )

#define AEHA_T 					( 425 )
#define AEHA_INTV 				( 130000 )
#define AEHA_HEADER_ONE_LEN 	( 8 )
#define AEHA_HEADER_ZERO_LEN 	( 4 )
#define AEHA_HEADER_LEN 		( AEHA_HEADER_ONE_LEN + AEHA_HEADER_ZERO_LEN )
#define AEHA_DEVICE_LEN 		( 16 )
#define AEHA_PARITY_LEN 		( 4 )
#define AEHA_DATA_LEN 			( 28 )
#define AEHA_TRAILER_LEN 		( 1 )

#define SONY_T 					( 600 )
#define SONY_INTV 				( 45000 )
#define SONY_HEADER_ONE_LEN 	( 4 )
#define SONY_HEADER_ZERO_LEN 	( 1 )
#define SONY_HEADER_LEN 		( SONY_HEADER_ONE_LEN + SONY_HEADER_ZERO_LEN )
#define SONY_DATA_LEN 			( 7 )
#define SONY_DEVICE_LEN 		( 13 )


#define _for(i, len)		for((i) = 0; (i) < (len); (i)++)

/**
 * @struct audir_stream
 * @brief implementation of audir_stream_t
 */
struct audir_stream {
	int out_id, in_id;
	int sample_rate;
	int frames_per_buffer;
	PaStream *out, *in;
	PaStreamParameters out_param, in_param;
	void (*poll_callback)(int format, unsigned int device, unsigned long data);
};

/**
 * @enum decode_status
 */
enum decode_status {
	DECODE_SUCCESS = 0,
	DECODE_ERROR = -1,
	DECODE_END = -2
};

/**
 * @struct decode_context
 * @brief (internal) decode context
 */
struct decode_context {
	int tcnt;			/** total sample count */
	int mcnt;			/** max trailer sample count */
	int tbit;			/** total decoded bit count */
	int status;
	int spp;
	int format;
	unsigned int device;
	unsigned int parity;
	unsigned long data;
	kvec_t(int) t;
};

/**
 * core of command generator and parser
 */
/**
 * @fn generate_audio_stream
 * @brief generate 44.1kHz-sampled 16-bit audio data from ASCII, encoded-bitstream.
 */
static
int16_t *generate_audio_stream(char const *encoded_bitstream, int us)
{
	int i, j;
	int16_t *d;
	int len;
	double spp = (double)us * (double)sps / 1000000.0;

	len = strlen(encoded_bitstream);
	d = (int16_t *)malloc(sizeof(int16_t) * ((int)(len * spp) + 1));
	for(i = 0, j = 0; i < len; i++) {
		for(; j < (int)((i+1) * spp); j++) {
			d[j] = (encoded_bitstream[i] == '0') ? INT16_MIN : INT16_MAX;
		}
	}
	d[j] = 0;
	return(d);
}

/**
 * @fn encode_bitstream
 */
static
char *encode_bitstream(char const *bitstream, char const *zero, char const *one)
{
	int i;
	char *eb, *p;
	int len;
	int spb = (strlen(one) > strlen(zero) ? strlen(one) : strlen(zero));

	len = strlen(bitstream);
	eb = p = (char *)malloc(sizeof(char) * spb * (len + 1));
	_for(i, len) {
		strcpy(p, bitstream[i] == '0' ? zero : one);
		p += strlen(bitstream[i] == '0' ? zero : one);
	}
	*p = '\0';
	return(eb);
}

/**
 * @fn generate_nec_stream
 * @brief generate audio stream of NEC command from id and data
 */
static
int16_t *generate_nec_stream(unsigned int device, unsigned long data)
{
	int i;
	char *encoded_data;
	kvec_t(char) e, b;
	int16_t *audio_data;

	/** generate bitstream of id and data section */
	kv_init(b);
	_for(i, NEC_DEVICE_LEN) { kv_push(b, (device>>i) & 0x01 ? '1' : '0'); }
	_for(i, NEC_DATA_LEN) { kv_push(b, (data>>i) & 0x01 ? '1' : '0'); }
	_for(i, NEC_DATA_LEN) { kv_push(b, (data>>i) & 0x01 ? '0' : '1'); }
	kv_push(b, '\0');
	encoded_data = encode_bitstream(kv_ptr(b), "10", "100");

	/** add header and trailer */
	kv_init(e);
	_for(i, NEC_HEADER_ONE_LEN) { kv_push(e, '1'); }
	_for(i, NEC_HEADER_ZERO_LEN) { kv_push(e, '0'); }
	_for(i, strlen(encoded_data)) { kv_push(e, encoded_data[i]); }
	kv_push(e, '1');

	/** add 108ms interval */
	_for(i, NEC_INTV / NEC_T) { kv_push(e, '0'); }
	kv_push(e, '\0');

	/** convert to audio stream */
	audio_data = generate_audio_stream(kv_ptr(e), NEC_T);

	/** clean buffers */
	kv_destroy(e);
	kv_destroy(b);
	free(encoded_data);
	return(audio_data);
}

/**
 * @fn generate_aeha_stream
 */
static
int16_t *generate_aeha_stream(unsigned int device, unsigned long data)
{
	int i;
	unsigned int parity = 0;
	char *encoded_data;
	kvec_t(char) e, b;
	int16_t *audio_data;

	/** calc parity */
	_for(i, 4) { parity = 0x0f & (parity ^ (device>>(4*i))); }

	/** generate bitstream of id and data section */
	kv_init(b);
	_for(i, AEHA_DEVICE_LEN) { kv_push(b, (device>>i) & 0x01 ? '1' : '0'); }
	_for(i, AEHA_PARITY_LEN) { kv_push(b, (parity>>i) & 0x01 ? '1' : '0'); }
	_for(i, AEHA_DATA_LEN) { kv_push(b, (data>>i) & 0x01 ? '1' : '0'); }
	kv_push(b, '\0');
	encoded_data = encode_bitstream(kv_ptr(b), "10", "100");

	/** add header and trailer */
	kv_init(e);
	_for(i, AEHA_HEADER_ONE_LEN) { kv_push(e, '1'); }
	_for(i, AEHA_HEADER_ZERO_LEN) { kv_push(e, '0'); }
	_for(i, strlen(encoded_data)) { kv_push(e, encoded_data[i]); }
	kv_push(e, '1');

	/** add 130ms interval */
	_for(i, AEHA_INTV / AEHA_T) { kv_push(e, '0'); }
	kv_push(e, '\0');

	/** convert to audio stream */
	audio_data = generate_audio_stream(kv_ptr(e), AEHA_T);

	/** clean buffers */
	kv_destroy(e);
	kv_destroy(b);
	free(encoded_data);
	return(audio_data);
}

/**
 * @fn generate_sony_stream
 */
static
int16_t *generate_sony_stream(unsigned int device, unsigned long data)
{
	int i;
	char *encoded_data;
	kvec_t(char) e, b;
	int16_t *audio_data;

	/** generate bitstream of id and data section */
	kv_init(b);
	_for(i, SONY_DATA_LEN) { kv_push(b, (data>>i) & 0x01 ? '1' : '0'); }
	_for(i, SONY_DEVICE_LEN) { kv_push(b, (device>>i) & 0x01 ? '1' : '0'); }
	kv_push(b, '\0');
	encoded_data = encode_bitstream(kv_ptr(b), "01", "011");

	/** add header and trailer */
	kv_init(e);
	_for(i, SONY_HEADER_ONE_LEN) { kv_push(e, '1'); }
	_for(i, strlen(encoded_data)) { kv_push(e, encoded_data[i]); }

	/** add 45ms interval */
	_for(i, SONY_INTV / SONY_T) { kv_push(e, '0'); }
	kv_push(e, '\0');

	/** convert to audio stream */
	audio_data = generate_audio_stream(kv_ptr(e), SONY_T);

	/** clean buffers */
	kv_destroy(e);
	kv_destroy(b);
	free(encoded_data);
	return(audio_data);
}

/**
 * @fn decode_start
 */
static
void decode_start(struct decode_context *d)
{
	d->tcnt = 0;
	d->mcnt = 10000 * sps / 1000000;
	d->tbit = 0;
	d->status = DECODE_SUCCESS;
	d->spp = -1;
	d->format = 0;
	d->device = 0;
	d->parity = 0;
	d->data = 0;
	kv_init(d->t);
	return;
}

/**
 * @fn decode_end
 */
static
void decode_end(struct decode_context *d)
{
	kv_destroy(d->t);
	return;
}

#define LAST(x)			( kv_at((x), kv_size(x)-1) )
#define SLAST(x)		( kv_at((x), kv_size(x)-2) )

/**
 * @fn decode_nec_stream
 */
static
void decode_nec_stream(struct decode_context *d, int cnt)
{
	int const base_tbit = 0;
	int const device_tbit = base_tbit + 2*NEC_DEVICE_LEN;
	int const data_tbit = device_tbit + 4*NEC_DATA_LEN;

	#define check_nec_bit_head(x) { \
		if(SLAST((x)->t) != 1) { (x)->status = DECODE_ERROR; } \
	}

	/** convert signal length to bit count */
	int c = kv_pop(d->t);
	kv_push(d->t, (c + d->spp/2) / d->spp);
	d->tbit++;

	/** decode */
	if(d->tbit <= device_tbit) {
		if(((d->tbit - base_tbit) % 2) == 1) { return; }
		check_nec_bit_head(d);
		d->device = (d->device>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(NEC_DEVICE_LEN-1));
	} else if(d->tbit <= data_tbit) {
		if(((d->tbit - device_tbit) % 2) == 1) { return; }
		check_nec_bit_head(d);
		d->data = (d->data>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(2*NEC_DATA_LEN-1));
	} else {
		/** check sanity */
		if((d->data & 0xff) == ((~d->data>>8) & 0xff)) {
			d->data &= 0xff;
		} else {
			d->status = DECODE_ERROR;
		}
		if(d->status == DECODE_ERROR) {
			d->format = -1;
			d->device = -1;
			d->data = -1;
		}
		d->status = DECODE_END;
	}

	#undef check_nec_bit_head
}

/**
 * @fn decode_aeha_stream
 */
static
void decode_aeha_stream(struct decode_context *d, int cnt)
{
	int i;
	unsigned int parity = 0;
	int const base_tbit = 0;
	int const device_tbit = base_tbit + 2*AEHA_DEVICE_LEN;
	int const parity_tbit = device_tbit + 2*AEHA_PARITY_LEN;
	int const data_tbit = parity_tbit + 2*AEHA_DATA_LEN;

	#define check_aeha_bit_head(x) { \
		if(SLAST((x)->t) != 1) { (x)->status = DECODE_ERROR; } \
	}

	/** convert signal length to bit count */
	int c = kv_pop(d->t);
	kv_push(d->t, (c + d->spp/2) / d->spp);
	d->tbit++;

	/** decode */
	if(d->tbit <= device_tbit) {
		if(((d->tbit - base_tbit) % 2) == 1) { return; }
		check_aeha_bit_head(d);
		d->device = (d->device>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(AEHA_DEVICE_LEN-1));
	} else if(d->tbit <= parity_tbit) {
		if(((d->tbit - device_tbit) % 2) == 1) { return; }
		check_aeha_bit_head(d);
		d->parity = (d->parity>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(AEHA_PARITY_LEN-1));
	} else if(d->tbit <= data_tbit) {
		if(((d->tbit - parity_tbit) % 2) == 1) { return; }
		check_aeha_bit_head(d);
		d->data = (d->data>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(AEHA_DATA_LEN-1));
	} else {
		/** check sanity */
		_for(i, 4) { parity = 0x0f & (parity ^ (d->device>>(4*i))); }
		if(parity != d->parity) {
			d->status = DECODE_ERROR;
		}
		if(d->status == DECODE_ERROR) {
			d->format = -1;
			d->device = -1;
			d->data = -1;
		}
		d->status = DECODE_END;
	}

	#undef check_aeha_bit_head
	return;
}

/**
 * @fn decode_sony_stream
 */
static
void decode_sony_stream(struct decode_context *d, int cnt)
{
	int const base_tbit = 0;
	int const data_tbit = base_tbit + 2*SONY_DATA_LEN;
	int const device_tbit = data_tbit + 2*SONY_DEVICE_LEN;

	#define check_sony_bit_head(x) { \
		if(SLAST((x)->t) != 1) { (x)->status = DECODE_ERROR; } \
	}

	/** convert signal length to bit count */
	int t = kv_pop(d->t);
	int c = (t + d->spp/2) / d->spp;
	kv_push(d->t, c);
	d->tbit++;

	/** decode */
	if(d->tbit <= data_tbit) {
		if(((d->tbit - base_tbit) % 2) == 1) { return; }
		check_sony_bit_head(d);
		d->data = (d->data>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(SONY_DATA_LEN-1));
	} else if(d->tbit <= device_tbit) {
		if(((d->tbit - data_tbit) % 2) == 1) { return; }
		check_sony_bit_head(d);
		d->device = (d->device>>1) | ((LAST(d->t) == 1 ? 0 : 1)<<(SONY_DEVICE_LEN-1));
	} else {
		if(d->status == DECODE_ERROR) {
			d->format = -1;
			d->device = -1;
			d->data = -1;
		}
		d->status = DECODE_END;
	}

	#undef check_sony_bit_head
	return;
}

/**
 * @fn detect_format
 */
static
void detect_format(struct decode_context *d, int cnt)
{
	struct _header {
		int format;
		int olen;
		int zlen;
		int hlen;
	};
	struct _header h[4] = {
		{ AUDIR_NEC,
		  NEC_T * NEC_HEADER_ONE_LEN * sps / 1000000,
		  NEC_T * NEC_HEADER_ZERO_LEN * sps / 1000000,
		  NEC_HEADER_LEN },
		{ AUDIR_AEHA,
		  AEHA_T * AEHA_HEADER_ONE_LEN * sps / 1000000,
		  AEHA_T * AEHA_HEADER_ZERO_LEN * sps / 1000000,
		  AEHA_HEADER_LEN },
		{ AUDIR_SONY,
		  SONY_T * SONY_HEADER_ONE_LEN * sps / 1000000,
		  SONY_T * SONY_HEADER_ZERO_LEN * sps / 1000000,
		  SONY_HEADER_LEN },
		{ 0, 0, 0, 0 }
	};
	struct _header *ph;

	/** detect format */
	if(kv_size(d->t) < 2) { return; }
	for(ph = h; ph->format != 0; ph++) {
		if(SLAST(d->t) > 0.8*ph->olen && SLAST(d->t) < 1.2*ph->olen
		 && LAST(d->t) > 0.8*ph->zlen && LAST(d->t) < 1.2*ph->zlen) {
			d->format = ph->format;
			d->spp = (d->tcnt = (SLAST(d->t) + LAST(d->t))) / ph->hlen;
			d->tbit = 0;
			if(d->format == AUDIR_SONY) {
				decode_sony_stream(d, cnt);
			}
		}
	}
	return;
}

#undef LAST
#undef SLAST

/**
 * @fn decode_bitstream
 */
static
int decode_bitstream(struct decode_context *d, int16_t signal)
{
	/** static */
	static int cnt = 0;
	static int16_t prev = -1;

	if(prev == signal) {
		cnt++;
		if(cnt > d->mcnt) { cnt = 0; return(DECODE_END); }
		return(DECODE_SUCCESS);
	}

	/** update spp and push bit length */
	kv_push(d->t, cnt);
	d->tcnt += cnt;

	/** parse bitstream */
	switch(d->format) {
		case AUDIR_NEC: decode_nec_stream(d, cnt); break;
		case AUDIR_AEHA: decode_aeha_stream(d, cnt); break;
		case AUDIR_SONY: decode_sony_stream(d, cnt); break;
		default: detect_format(d, cnt); break;
	}

	/** clear counter for the next bit */
	cnt = 1; prev = signal;
	return(d->status);
}

/**
 * @fn decode_audio_stream
 */
static
int decode_audio_stream(
	const void *ib,
	void *ob,
	unsigned long fpb,
	const PaStreamCallbackTimeInfo* ti,
	PaStreamCallbackFlags stat,
	void *vs)
{
	/** (const) states and transition matrix */
	enum _signal { INIT = -1, IDLE = 0, CLR = 1, SET = 2 };
	enum _bin { LOW = 0, MARGIN = 1, HIGH = 2 };
	int const trans[3][3] = {
		{IDLE, CLR, CLR},		/** low */
		{IDLE, CLR, SET},		/** margin */
		{SET, SET, SET}			/** high */
	};
	/** (static) state and baseline */
	static int state = INIT;
	static int thresh = 1000, margin = 100;
	static struct decode_context ctx;
	/** local variables */
	int i;
	int16_t const *in = (int16_t const *)ib;
	struct audir_stream const *s = (struct audir_stream const *)vs;

	/** initialize */
	if(state == INIT) {
		state = IDLE;
		decode_start(&ctx);
	}

	/** parse input signal */
	_for(i, fpb) {
		int bin;
		if(in[i] < thresh - margin) { bin = LOW; }
		else if(in[i] < thresh + margin) { bin = MARGIN; }
		else { bin = HIGH; }
		state = trans[bin][state];		/** signal state transition */

		/** update thresh and margin */

		/** decode */
		if(state == IDLE) { continue; }
		if(decode_bitstream(&ctx, state) != DECODE_END) { continue; }

		/** decode finished */
		decode_end(&ctx);
		state = IDLE;					/** clear signal state */

		/** dispatch callback */
		if(s->poll_callback != NULL && ctx.format != -1) {
			s->poll_callback(
				ctx.format,
				ctx.device,
				ctx.data);
		}
		decode_start(&ctx);				/** wait the next signal */
	}
	return 0;
}

/**
 * high-level APIs
 */
/**
 * @fn audir_init
 */
int audir_init(void)
{
	return(Pa_Initialize() == paNoError ? AUDIR_SUCCESS : AUDIR_ERROR);
}

/**
 * @fn audir_close
 */
int audir_close(void)
{
	return(Pa_Terminate() == paNoError ? AUDIR_SUCCESS : AUDIR_ERROR);
}

/**
 * @fn audir_get_port_list
 */
audir_port_list_t const *audir_get_port_list(void)
{
	int i, cnt;
	PaDeviceInfo const *info;
	struct audir_port_list *l, *pl;

	/** list port */
	if((cnt = Pa_GetDeviceCount()) <= 0) {
		return(NULL);
	}

	/** malloc list */
	l = pl = (struct audir_port_list *)malloc(sizeof(struct audir_port_list) * (2 * cnt + 1));

	/** enumerate output devices */
	_for(i, cnt) {
		info = Pa_GetDeviceInfo(i);
		if(info->maxOutputChannels > 0) {
			pl->name = info->name;		/** shallow copy, depends on const type */
			pl->direction = AUDIR_OUT;
			pl->id = out_id_base + i;
			pl++;
		}
	}
	/** enumerate input devices */
	_for(i, cnt) {
		info = Pa_GetDeviceInfo(i);
		if(info->maxInputChannels > 0) {
			pl->name = info->name;
			pl->direction = AUDIR_IN;
			pl->id = in_id_base + i;
			pl++;
		}
	}
	/** add terminator */
	pl->name = NULL; pl->direction = 0; pl->id = 0;
	return(l);
}

/**
 * @fn audir_free_port_list
 */
int audir_free_port_list(struct audir_port_list const *l)
{
	struct audir_port_list *ll = (struct audir_port_list *)l;
	if(ll != NULL) {
		free(ll);
	}
	return(AUDIR_SUCCESS);
}

/**
 * @fn audir_open_port
 */
struct audir_stream const *audir_open_port(int out_id, int in_id)
{
	int cnt, err;
	struct audir_stream *s;

	/** check if the both ids are out of range */
	if(out_id < out_id_base && in_id < in_id_base) {
		return(NULL);
	}

	/** init vals */
	cnt = Pa_GetDeviceCount();
	s = (struct audir_stream *)malloc(sizeof(struct audir_stream));
	memset(s, 0, sizeof(struct audir_stream));
	s->sample_rate = sps;
	s->frames_per_buffer = paFramesPerBufferUnspecified;

	/** set output port info */
	if(out_id >= out_id_base && (out_id - out_id_base) < cnt) {
		s->out_id = out_id;
		s->out_param.channelCount = 1;		/** monoral */
		s->out_param.device = out_id - out_id_base;
		s->out_param.hostApiSpecificStreamInfo = NULL;
		s->out_param.sampleFormat = paInt16;
		s->out_param.suggestedLatency = Pa_GetDeviceInfo(s->out_param.device)->defaultLowOutputLatency;
		s->out_param.hostApiSpecificStreamInfo = NULL;
		err = Pa_OpenStream(
			&(s->out),
			NULL,
			&(s->out_param),
			s->sample_rate,
			s->frames_per_buffer,
			paNoFlag,
			NULL,
			(void *)s);

		if(err != paNoError) {
			free(s); return(NULL);
		}
	} else {
		out_id = 0;
		s->out = NULL;
	}

	/** set input port info */
	if(in_id >= in_id_base && (in_id - in_id_base) < cnt) {
		s->in_id = in_id;
		s->in_param.channelCount = 1;
		s->in_param.device = in_id - in_id_base;
		s->in_param.hostApiSpecificStreamInfo = NULL;
		s->in_param.sampleFormat = paInt16;
		s->in_param.suggestedLatency = Pa_GetDeviceInfo(s->in_param.device)->defaultLowInputLatency;
		s->in_param.hostApiSpecificStreamInfo = NULL;
		err = Pa_OpenStream(
			&(s->in),
			&(s->in_param),
			NULL,
			s->sample_rate,
			s->frames_per_buffer,
			paNoFlag,
			decode_audio_stream,
			(void *)s);

		if(err != paNoError) {
			free(s); return(NULL);
		}
	} else {
		in_id = 0;
		s->in = NULL;
	}
	return(s);
}

/**
 * @fn audir_close_port
 */
int audir_close_port(struct audir_stream const *s)
{
	int err = AUDIR_SUCCESS;
	struct audir_stream *ss = (struct audir_stream *)s;
	if(ss != NULL) {
		if(ss->out != NULL && Pa_CloseStream(ss->out) != paNoError) {
			err |= AUDIR_ERROR;
		}
		if(ss->in != NULL && Pa_CloseStream(ss->in) != paNoError) {
			err |= AUDIR_ERROR;
		}
		free(ss);
	}
	return(err);
}

/**
 * @fn audir_send
 */
int audir_send(
	struct audir_stream const *s,
	int format,
	unsigned int device,
	unsigned long data,
	int repeat)
{
	int i, err;
	int16_t *p;
	int len = 0;
	struct audir_stream *ss = (struct audir_stream *)s;

	if(ss == NULL) {
		return(AUDIR_ERROR);
	}

	/** generate audio stream */
	switch(format) {
		case AUDIR_NEC: p = generate_nec_stream(device, data); break;
		case AUDIR_AEHA: p = generate_aeha_stream(device, data); break;
		case AUDIR_SONY: p = generate_sony_stream(device, data); break;
		default: return(AUDIR_ERROR);
	}

	/** calculate length */
	while(p[len] != 0) {
		if(p[len] < 0) { p[len] = 0; }
		len++;
	}

	/** start output */
	Pa_StartStream(ss->out);
	_for(i, repeat) { err = Pa_WriteStream(ss->out, (void *)p, len); }
	Pa_StopStream(ss->out);

	/** finish */
	free(p);
	return(err == paNoError ? AUDIR_SUCCESS : AUDIR_ERROR);
}

/**
 * @fn audir_start_poll
 * @brief start polling, enable dispatching when valid command arrived.
 */
int audir_start_poll(
	struct audir_stream const *s,
	void (*poll_callback)(int format, unsigned int device, unsigned long data))
{
	struct audir_stream *ss = (struct audir_stream *)s;
	ss->poll_callback = poll_callback;
	return(Pa_StartStream(s->in) == paNoError ? AUDIR_SUCCESS : AUDIR_ERROR);
}

/**
 * @fn audir_stop_poll
 */
int audir_stop_poll(
	audir_stream_t const *s)
{
	return(Pa_StopStream(s->in) == paNoError ? AUDIR_SUCCESS : AUDIR_ERROR);
}

/**
 * end of audir.c
 */
