#! /usr/bin/env python
# encoding: utf-8

def options(opt):
	opt.load('compiler_c')

def configure(conf):
	conf.load('compiler_c')

	conf.check_cc(lib = 'portaudio')

	conf.env.append_value('CFLAGS', '-Wall')
	conf.env.append_value('CFLAGS', '-fPIC')
	conf.env.append_value('CFLAGS', '-O3')
	# conf.env.append_value('CFLAGS', '-g')

def build(bld):
	bld.stlib(
		source = 'audir.c',
		target = 'audir',
		lib = 'portaudio')
	"""
	bld.shlib(
		source = 'audir.c',
		target = 'audir',
		lib = 'portaudio')
	"""
	bld.program(
		source = 'ir.c',
		target = 'ir',
		lib = 'portaudio',
		use = 'audir')

