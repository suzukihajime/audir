
/**
 * @file audir.h
 *
 * @brief AudIR :: audio-based IR command generator and parser
 *
 * @author Hajime Suzuki
 * @date 2015/6/27
 * @license Apache v2.
 */

#ifndef _AUDIR_H_INCLUDED
#define _AUDIR_H_INCLUDED

/**
 * @type audir_stream_t
 * @brief context container
 */
typedef struct audir_stream audir_stream_t;

/**
 * @enum _audir_error_flags
 */
enum _audir_error_flags {
	AUDIR_SUCCESS = 0,
	AUDIR_ERROR = 1
};

/**
 * @enum _audir_format_flags
 * @brief IR command format flags given to audir_init.
 */
enum _audir_format_flags {
	AUDIR_NEC 		= 1,
	AUDIR_AEHA 		= 2,
	AUDIR_SONY 		= 4
};

/**
 * @enum _audir_direction_flags
 */
enum _audir_direction_flags {
	AUDIR_OUT 		= 1,
	AUDIR_IN 		= 2
};

/**
 * @type audir_poll_callback
 */
typedef
int audir_poll_callback(
	int format,
	unsigned int device,
	unsigned long data);

/**
 * @struct audir_port_list
 */
struct audir_port_list {
	char const *name;
	int direction;
	int id;
};
typedef struct audir_port_list audir_port_list_t;

/**
 * @fn audir_init
 */
int audir_init(void);

/**
 * @fn audir_get_port_list
 */
audir_port_list_t const *audir_get_port_list(void);

/**
 * @fn audir_free_port_list
 */
int audir_free_port_list(audir_port_list_t const *list);

/**
 * @fn audir_open_port
 */
audir_stream_t const *audir_open_port(int out_id, int in_id);

/**
 * @fn audir_send
 */
int audir_send(
	audir_stream_t const *stream,
	int format,
	unsigned int device,
	unsigned long data,
	int repeat);

/**
 * @fn audir_start_poll
 */
int audir_start_poll(
	audir_stream_t const *stream,
	void (*poll_callback)(int format, unsigned int device, unsigned long data));

/**
 * @fn audir_stop_poll
 */
int audir_stop_poll(
	audir_stream_t const *stream);

/**
 * @fn audir_close_port
 */
int audir_close_port(audir_stream_t const *stream);

/**
 * @fn audir_close
 */
int audir_close(void);

#endif /* #ifndef _AUDIR_H_INCLUDED */
/**
 * end of audir.h
 */
