
# AudIR :: audio-based IR command generator / parser

AudIR library is a audio-based IR remote controller command generator.
The library can generate the NEC, AEHA, and SONY format IR commands,
send them via audio output.

## Usage

### Library

```
// init context
audir_init();
audir_stream_t const *s = audir_open_port(audio_out_id, audio_in_id);

// send
audir_send(s, AUDIR_AEHA, device, data, repeat);

// receive
void callback_function(int format, unsigned int device, unsigned long data) { ... }
audir_start_poll(s, callback_function);
audir_stop_poll(s);

// close
audir_close_port(s);
audir_close();
```

### ir command

```
# list up audio ports
$ ir list
ID   DIR   NAME
002  OUT   Built-in Output
101   IN   Built-in Microph
:

# send (device = 23, data = 14, via audio port 2)
$ ir send -d2 AEHA 23 14

# poll audio port
$ ir poll -d101
AEHA 49 199
AEHA 49 197
:
```

## Build

Requires python 2.7 or 3.3, and portaudio.

```
$ ./waf configure
$ ./waf build
```
