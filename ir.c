
/**
 * @file irsend.c
 *
 * @brief send IR commands with libaudir
 *
 * @author Hajime Suzuki
 * @date 2015/6/27
 * @license Apache v2.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include "audir.h"

static
char const *help =
	"\n"
	"  ir -- send and receive IR commands via audio port\n"
	"\n"
	"  usage: $ ir <command> [arguments and options]\n"
	"\n"
	"  commands:\n"
	"    list      list audio devices\n"
	"    send      send data (arguments = <format> <device_id> <data>)\n"
	"      format options: NEC / AEHA / SONY\n"
	"    poll      poll port\n"
	"\n"
	"  options and defaults:\n"
	"    -d <int>  specify audio device by id\n"
	"    -n <int>  #repeats                             (1)\n"
	"    -h        show this message\n"
	"\n"
	"  examples:\n"
	"    $ ir send NEC 0x15 0x62 -d1 -n3\n"
	"    $ ir poll -d102\n"
	"\n";

enum _mode {
	LIST = 1,
	SEND = 2,
	POLL = 3
};

/** poll flag */
static volatile int polling = 0;

/** parse number */
long ntoi(char const *str)
{
	if(*str == '0') {
		if(*++str == 'x') {
			return(strtol(++str, NULL, 16));
		} else {
			return(strtol(str, NULL, 8));
		}
	} else {
		return(atoi(str));
	}
}

int main_list(int argc, char *argv[])
{
	/** enumerate devices */
	audir_port_list_t const *l, *p;

	audir_init();
	l = audir_get_port_list();
	printf("ID   DIR   NAME\n");
	for(p = l; p->name != NULL; p++) {
		printf("%03d  %3s   %s\n", p->id, p->direction == AUDIR_IN ? "IN" : "OUT", p->name);
	}
	audir_free_port_list(l);
	audir_close();
	return 0;
}

int main_send(int argc, char *argv[])
{
	int c, cnt = 0, repeat = 1, audio_id = -1, format_id = -1;
	char *format = "AEHA";
	unsigned int device;
	unsigned long data;
	audir_stream_t const *s;

	/** parse options */
	while(optind < argc) {
		if((c = getopt(argc, argv, "d:n:")) != -1) {
			switch(c) {
			case 'd': audio_id = atoi(optarg); break;
			case 'n': repeat = atoi(optarg); break;
			default:
				fprintf(stderr, "[ERROR] Unknown option: `%c'.\n", c);
				exit(1);
			}
		} else {
			switch(cnt++) {
			case 0: format = argv[optind++]; break;
			case 1: device = atoi(argv[optind++]); break;
			case 2: data = atoi(argv[optind++]); break;
			default:
				fprintf(stderr, "[ERROR] Too many arguments.\n");
				exit(1);
			}
		}
	}
	if(cnt != 3) {
		fprintf(stderr, "[ERROR] Too few arguments.\n");
		exit(1);
	}

	audir_init();
	if(audio_id == -1) {
		audir_port_list_t const *l, *p;
		l = audir_get_port_list();
		for(p = l; p->name != NULL; p++) {
			if(p->direction == AUDIR_OUT) {
				audio_id = p->id; break;
			}
		}
		audir_free_port_list(l);
	}
	
	/** parse format string */
	if(strcmp("NEC", format) == 0) {
		format_id = AUDIR_NEC;
	} else if(strcmp("AEHA", format) == 0) {
		format_id = AUDIR_AEHA;
	} else if(strcmp("SONY", format) == 0) {
		format_id = AUDIR_SONY;
	}

	/** send */
	s = audir_open_port(audio_id, 0);
	audir_send(s, format_id, device, data, repeat);
	audir_close_port(s);
	audir_close();
	return 0;
}

void interrupt(int signal)
{
	if(signal == SIGINT) {
		polling = 0;
	}
	return;
}

void callback(int format, unsigned int device, unsigned long data)
{
	char const *format_str;
	switch(format) {
		case AUDIR_NEC: format_str = "NEC"; break;
		case AUDIR_AEHA: format_str = "AEHA"; break;
		case AUDIR_SONY: format_str = "SONY"; break;
		default: format_str = ""; break;
	}
	printf("%s %u %lu\n", format_str, device, data);
	return;
}

int main_poll(int argc, char *argv[])
{
	int c, audio_id = -1;
	audir_stream_t const *s;

	/** parse options */
	while((c = getopt(argc, argv, "d:")) != -1) {
		switch(c) {
			case 'd': audio_id = atoi(optarg); break;
			default:
				fprintf(stderr, "[ERROR] Unknown option: `%c'.\n", c);
				exit(1);
		}
	}
	if(optind != argc) {
		fprintf(stderr, "[ERROR] Too many arguments.\n");
		exit(1);
	}

	audir_init();
	if(audio_id == -1) {
		audir_port_list_t const *l, *p;
		l = audir_get_port_list();
		for(p = l; p->name != NULL; p++) {
			if(p->direction == AUDIR_IN) {
				audio_id = p->id; break;
			}
		}
		audir_free_port_list(l);
	}

	/** poll */
	s = audir_open_port(0, audio_id);
	audir_start_poll(s, callback);
	if(signal(SIGINT, interrupt) != SIG_ERR) {
		polling = 1;
		while(polling) {}
	}
	audir_stop_poll(s);
	audir_close_port(s);
	audir_close();
	return 0;
}

int main(int argc, char *argv[])
{
	if(argc == 1) {
		fprintf(stderr, "%s", help);
		exit(1);
	}
	if(strcmp(argv[1], "list") == 0) {
		return(main_list(--argc, ++argv));
	} else if(strcmp(argv[1], "send") == 0) {
		return(main_send(--argc, ++argv));
	} else if(strcmp(argv[1], "poll") == 0) {
		return(main_poll(--argc, ++argv));
	} else {
		fprintf(stderr, "%s", help);
		exit(1);
	}
	return 0;
}

/**
 * end of irsend.c
 */
